package ru.t1.fpavlov.tm.exception.field;

/**
 * Created by fpavlov on 20.12.2021.
 */
public final class EmailEmptyException extends AbstractFieldException {

    public EmailEmptyException() {
        super("Error! E-mail is empty");
    }

}
