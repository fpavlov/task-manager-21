package ru.t1.fpavlov.tm.exception.user;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class IncorrectLoginOrPasswordException extends AbstractUserException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Login or password is incorrect");
    }

}
