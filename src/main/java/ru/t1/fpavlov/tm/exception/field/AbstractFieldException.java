package ru.t1.fpavlov.tm.exception.field;

import ru.t1.fpavlov.tm.exception.AbstractException;

/**
 * Created by fpavlov on 06.12.2021.
 */
public class AbstractFieldException extends AbstractException {

    public AbstractFieldException() {
    }

    public AbstractFieldException(String message) {
        super(message);
    }

    public AbstractFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldException(Throwable cause) {
        super(cause);
    }

    public AbstractFieldException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
