package ru.t1.fpavlov.tm.exception.system;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class CommandIncorrectException extends AbstractSystemException {

    public CommandIncorrectException() {
        super("Error! Incorrect command.");
    }

    public CommandIncorrectException(final String command) {
        super("Error! Command " + command + " is incorrect.");
    }

}
