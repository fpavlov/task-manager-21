package ru.t1.fpavlov.tm.exception.user;

/**
 * Created by fpavlov on 20.12.2021.
 */
public final class RoleEmptyException extends AbstractUserException {

    public RoleEmptyException() {
        super("Error! Role is empty");
    }

}