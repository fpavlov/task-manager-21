package ru.t1.fpavlov.tm.exception.entity;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class TaskNotFoundException extends AbstractEntityNotFoundException {

    public TaskNotFoundException() {
        super("Error! The task wasn't found. Correct parameters of your request");
    }

}
