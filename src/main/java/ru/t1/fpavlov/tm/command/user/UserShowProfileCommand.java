package ru.t1.fpavlov.tm.command.user;

import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class UserShowProfileCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Display user profile info";

    public static final String NAME = "user-profile";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final User user = this.getAuthService().getUser();
        this.renderUser(user);
    }

}
