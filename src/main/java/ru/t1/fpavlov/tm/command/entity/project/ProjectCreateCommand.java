package ru.t1.fpavlov.tm.command.entity.project;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectCreateCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Create new project";

    public static final String NAME = "project-create";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final String name = this.askEntityName();
        final String description = this.askEntityDescription();
        final String userId = this.getUserId();
        this.getProjectService().create(userId, name, description);
    }

}
