package ru.t1.fpavlov.tm.command.user;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Update user profile";

    public static final String NAME = "update-profile";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final String userId = this.getAuthService().getUserId();
        final String firstName = this.input("First name:");
        final String lastName = this.input("Last name:");
        final String middleName = this.input("Middle name:");
        this.getServiceLocator().getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

}
