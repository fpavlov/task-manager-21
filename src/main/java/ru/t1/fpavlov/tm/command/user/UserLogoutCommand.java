package ru.t1.fpavlov.tm.command.user;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Logout";

    public static final String NAME = "logout";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        this.getAuthService().logout();
    }

}
