package ru.t1.fpavlov.tm.api.model;

/**
 * Created by fpavlov on 07.12.2021.
 */
public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute();

}
