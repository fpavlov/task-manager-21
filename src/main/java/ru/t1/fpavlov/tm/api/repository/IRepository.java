package ru.t1.fpavlov.tm.api.repository;

import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

/**
 * Created by fpavlov on 21.12.2021.
 */
public interface IRepository<M extends AbstractModel> {

    void clear();

    int getSize();

    List<M> findAll();

    List<M> findAll(final Comparator comparator);

    List<M> findAll(Sort sort);

    M add(final M item);

    M findById(final String id);

    M findByIndex(final Integer index);

    M remove(final M item);

    M removeById(final String id);

    M removeByIndex(final Integer index);

    boolean isIdExist(final String id);

}
