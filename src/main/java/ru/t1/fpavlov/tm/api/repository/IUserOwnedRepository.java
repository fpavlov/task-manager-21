package ru.t1.fpavlov.tm.api.repository;

import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

/**
 * Created by fpavlov on 13.01.2022.
 */
public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(final String userId);

    List<M> findAll(final String userId);

    List<M> findAll(final String userId, final Comparator comparator);

    List<M> findAll(final String userId, final Sort sort);

    boolean existsById(final String userId, final String id);

    M findById(final String userId, final String id);

    M findByIndex(final String userId, final Integer index);

    int getSize(final String userId);

    M removeById(final String userId, final String id);

    M removeByIndex(final String userId, final Integer index);

    M add(final String userId, final M item);

    M remove(final String userId, final M item);

}
