package ru.t1.fpavlov.tm.service;


import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.api.service.ITaskService;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.exception.entity.TaskNotFoundException;
import ru.t1.fpavlov.tm.exception.field.DescriptionEmptyException;
import ru.t1.fpavlov.tm.exception.field.IdEmptyException;
import ru.t1.fpavlov.tm.exception.field.IndexIncorrectException;
import ru.t1.fpavlov.tm.exception.field.NameEmptyException;
import ru.t1.fpavlov.tm.exception.user.UserIdEmptyException;
import ru.t1.fpavlov.tm.model.Task;

import java.util.Collections;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return this.repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return this.repository.create(userId, name, description);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return this.repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId, final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (sort == null) return this.repository.findAllByProjectId(userId, projectId);
        return this.repository.findAllByProjectId(userId, projectId, sort.getComparator());
    }

    @Override
    public Task update(final String userId, final Task task, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        if (name == null) throw new NameEmptyException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task item = this.findById(id);
        if (item == null) throw new TaskNotFoundException();
        item.setName(name);
        item.setDescription(description);
        return item;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= this.repository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task item = this.findByIndex(index);
        if (item == null) throw new TaskNotFoundException();
        item.setName(name);
        item.setDescription(description);
        return item;
    }

    @Override
    public Task changeStatus(final String userId, final Task task, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        if (status == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) return null;
        final Task item = this.findById(userId, id);
        if (item == null) throw new TaskNotFoundException();
        item.setStatus(status);
        return item;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= this.repository.getSize()) throw new IndexIncorrectException();
        if (status == null) return null;
        final Task item = this.findByIndex(userId, index);
        if (item == null) throw new TaskNotFoundException();
        item.setStatus(status);
        return item;
    }

}
