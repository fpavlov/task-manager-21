package ru.t1.fpavlov.tm.service;

import ru.t1.fpavlov.tm.api.repository.ICommandRepository;
import ru.t1.fpavlov.tm.api.service.ICommandService;
import ru.t1.fpavlov.tm.command.AbstractCommand;

import java.util.Collection;

/*
 * Created by fpavlov on 06.10.2021.
 */
public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        this.commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argumentName) {
        if (argumentName == null || argumentName.isEmpty()) return null;
        return this.commandRepository.getCommandByArgument(argumentName);
    }

    @Override
    public AbstractCommand getCommandByName(final String commandName) {
        if (commandName == null || commandName.isEmpty()) return null;
        return this.commandRepository.getCommandByName(commandName);
    }

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return this.commandRepository.getTerminalCommands();
    }

}
