package ru.t1.fpavlov.tm.service;

import ru.t1.fpavlov.tm.api.repository.IRepository;
import ru.t1.fpavlov.tm.api.service.IService;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.exception.field.IdEmptyException;
import ru.t1.fpavlov.tm.exception.field.IndexIncorrectException;
import ru.t1.fpavlov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

/**
 * Created by fpavlov on 23.12.2021.
 */
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    AbstractService(final R repository) {
        this.repository = repository;
    }

    public M add(final M item) {
        if (item == null) return null;
        return this.repository.add(item);
    }

    public void clear() {
        this.repository.clear();
    }

    public M remove(final M item) {
        if (item == null) return null;
        return this.repository.remove(item);
    }

    public List<M> findAll() {
        return this.repository.findAll();
    }

    @Override
    public List<M> findAll(Comparator comparator) {
        if (comparator == null) return this.findAll();
        return this.repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(Sort sort) {
        if (sort == null) return this.findAll();
        return findAll(sort.getComparator());
    }

    public M findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return this.repository.findById(id);
    }

    @Override
    public M findByIndex(final Integer index) {
        if (index == null || index < 0) throw new IdEmptyException();
        if (index >= this.repository.getSize()) throw new IndexIncorrectException();
        return this.repository.findByIndex(index);
    }

    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return this.repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M item = this.findByIndex(index);
        if (item == null) return null;
        return this.repository.removeByIndex(index);
    }

    public boolean isIdExist(final String id) {
        return this.findById(id) != null;
    }

    public int getSize() {
        return this.repository.getSize();
    }

}
