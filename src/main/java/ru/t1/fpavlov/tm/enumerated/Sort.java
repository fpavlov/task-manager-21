package ru.t1.fpavlov.tm.enumerated;

import ru.t1.fpavlov.tm.comparator.CreatedComparator;
import ru.t1.fpavlov.tm.comparator.NameComparator;
import ru.t1.fpavlov.tm.comparator.StatusComparator;

import java.util.Comparator;

/**
 * Created by fpavlov on 26.11.2021.
 */
public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    private final String displayName;

    private final Comparator comparator;

    Sort(final String displayName, final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public Comparator getComparator() {
        return this.comparator;
    }

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Sort item : values()) {
            if (value.equals(item.getDisplayName())) return item;
        }
        return null;
    }

    public static String[] displayValues() {
        final Sort sortNames[] = Sort.values();
        final String sortValues[] = new String[sortNames.length];
        for (int i = 0; i < sortValues.length; i++) {
            sortValues[i] = sortNames[i].getDisplayName();
        }
        return sortValues;
    }

}
